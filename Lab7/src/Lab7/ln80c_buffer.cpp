//
// Created by timur on 14.11.2019.
//

#include "ln80c_buffer.h"

int ln80c_buffer::sync() {
    std::string str = std::to_string(_count);
    str.push_back(')');

    _buff->sputn(str.c_str(), str.length());
    _buff->sputn(_line, pptr() - _line);

    pbump(-int(_buffSize));
    ++_count;
    return 0;
}

int ln80c_buffer::overflow(int c) {
    if (c != traits_type::eof()){
        *pptr() = c;
        pbump(1);
        sync();
        _buff->sputc('\n');
        return c;
    }

    return traits_type::eof();
}

ln80c_buffer::ln80c_buffer(std::streambuf *buff): _buff(buff) {
    _line = new char[_buffSize];
    setp(_line, _line+_buffSize-1);
}
