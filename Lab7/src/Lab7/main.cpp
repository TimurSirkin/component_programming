#include "ln80c_buffer.h"
#include "stat_buffer.h"

int main() {
    stat_buffer buf;
    std::ostream out(&buf);
    std::string str = "Hello world!\nHow are you?";
    out << str << "\n\n";
    out.rdbuf()->pubsync();
    buf.printStat();
    return 0;
}