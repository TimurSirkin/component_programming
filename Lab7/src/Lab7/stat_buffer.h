//
// Created by timur on 26.11.2019.
//

#ifndef LAB7_STAT_BUFFER_H
#define LAB7_STAT_BUFFER_H

#include <iostream>

class stat_buffer : public std::streambuf {
public:
    stat_buffer();
    void printStat();
protected:
    int overflow(int c) override;
    int sync() override;
private:
    unsigned int _lineCount = 1;
    unsigned int _wordCount = 0;
    unsigned int _symbolCount = 0;
    static const size_t _buffSize = 72;
    char _buff[_buffSize]{};
    bool isAfterWord = false;
};


#endif //LAB7_STAT_BUFFER_H
