//
// Created by timur on 26.11.2019.
//

#include <cctype>
#include "stat_buffer.h"

int stat_buffer::overflow(int c) {
    if (c != traits_type::eof()){
        *pptr() = c;
        pbump(1);
        sync();
        return c;
    }

    return traits_type::eof();
}

int stat_buffer::sync() {
    char *ptr = _buff;
    while (*ptr != '\0'){
        std::cout << *ptr;
        if (std::isblank(*ptr) && isAfterWord){
            ++_wordCount;
            isAfterWord = false;
            //std::cout << " word\n";
        }
        else if (*ptr == '\n'){
            if (isAfterWord){
                ++_wordCount;
                //std::cout << " word\n";
            }
            ++_lineCount;
            isAfterWord = true;
            //std::cout << " line\n";
        }
        else {
            ++_symbolCount;
            isAfterWord = true;
            //std::cout << " symbol\n";
        }
        ++ptr;
    }

    if(isAfterWord){
        ++_wordCount;
    }

    pbump(-int(_buffSize));
    return 0;
}

stat_buffer::stat_buffer() {
    setp(_buff, _buff+_buffSize-1);
}

void stat_buffer::printStat() {
    std::cout << "Symbols count: " << _symbolCount << "\n";
    std::cout << "Words count: " << _wordCount << "\n";
    std::cout << "Lines count: " << _lineCount << "\n";
}
