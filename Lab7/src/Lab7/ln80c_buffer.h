#ifndef LAB7_LN80C_BUFFER_H
#define LAB7_LN80C_BUFFER_H

#include <iostream>
#include <sstream>
#include <vector>

class ln80c_buffer : public std::streambuf{
public:
    explicit ln80c_buffer(std::streambuf *buff);
protected:
    int overflow(int c) override ;
    int sync() override;
private:
    static const size_t _buffSize = 72;
    char *_line;
    unsigned int _count = 1;
    std::streambuf *_buff;
};


#endif //LAB7_LN80C_BUFFER_H
