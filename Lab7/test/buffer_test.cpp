#include<gtest/gtest.h>
#include<iostream>
#include <sstream>
#include"../src/Lab7/ln80c_buffer.h"

TEST(Buffer, Compare) {

     std::string str = 
    "Heloooooooooooooooooooooooooooooo wooooooooooooooooooooooooooooooooooooo"
    "rld, hooooooooooooooooow aaaaaaaaaaaaaaaaaare youuuuuuuuuuuuuuuuuu???? I"
    "M FIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIINE!";

     std::string targetStr = 
    "1)Heloooooooooooooooooooooooooooooo wooooooooooooooooooooooooooooooooooooo\n"
    "2)rld, hooooooooooooooooow aaaaaaaaaaaaaaaaaare youuuuuuuuuuuuuuuuuu???? I\n"
    "3)M FIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIINE!";

    std::stringbuf stream;
    ln80c_buffer buf(&stream);
    std::ostream out(&buf);
    out << str;
    out.rdbuf()->pubsync();
    str = stream.str();

    EXPECT_EQ(targetStr, str);
}