//
// Created by timur on 22.10.2019.
//

#ifndef LAB6_RECTANGLE_H
#define LAB6_RECTANGLE_H

template<typename T>
class Rectangle {
public:
    Rectangle(T length, T width);

    void setLength(T length);
    T getLength();
    void setWidth(T width);
    T getWidth();

    T getArea();
private:
    T _length;
    T _width;
};

template<typename T>
void Rectangle<T>::setLength(T length) {
    _length = length;
}

template<typename T>
T Rectangle<T>::getLength() {
    return _length;
}

template<typename T>
void Rectangle<T>::setWidth(T width) {
    _width = width;
}

template<typename T>
T Rectangle<T>::getWidth() {
    return _width;
}

template<typename T>
T Rectangle<T>::getArea() {
    return _width * _length;
}

template<typename T>
Rectangle<T>::Rectangle(T length, T width) {
    _length = length;
    _width = width;
}


#endif //LAB6_RECTANGLE_H
