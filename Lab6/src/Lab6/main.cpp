#include <iostream>
#include "Rectangle.h"
#include "Circle.h"
#include "Trait.h"
#include "Polynomial.h"

#define comp std::complex<double>

int main() {
    Rectangle<double> rect(2, 10);
    Circle<double> circle(10);
    std::cout << "Rectangle size: " << Trait<Rectangle<double>>::size(rect) << "\n";
    std::cout << "Circle size: " << Trait<Circle<double>>::size(circle) << "\n\n";

    Polynomial<int>
    polR({1, 1, 2, 3});
    polR.print();
    polR.shura().print();

    std::cout << "\n";

    Polynomial<comp>
    polI({1. + 1i, 1. + 1i, 2. + 2i, 3. + 3i});
    polI.print();
    polI.shura().print();

    return 0;
}