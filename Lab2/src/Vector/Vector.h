//
// Created by timur on 20.09.2019.
//

#ifndef VECTOR_VECTOR_H
#define VECTOR_VECTOR_H

#include <iostream>
#include <tuple>

template<class T> class Vector{
public:
    Vector();
    Vector(const Vector<T> &vector);
    Vector(Vector<T> &&vector) noexcept;
    ~Vector();

    size_t size();

    T *begin();
    T *end();

    void resize(size_t newSize);

    void push_back(const T &element);
    void push_back(T &&element);
    void erase(T *from, T *to);
    void pop_back();

    friend std::ostream& operator<< (std::ostream &out, const Vector<int> &vector);
    friend std::ostream& operator<< (std::ostream &out, const Vector<std::string> &vector);
    T &operator[](size_t index);

    Vector<T> &operator=(const Vector<T> &vector);
    Vector<T> &operator=(Vector<T> &&vector) noexcept;

    void getInfo();
private:
    T *data;
    size_t _size;

    void swap(Vector<T> &vector);
    void swap(Vector<T> &&vector);
};

template<class T>
void swap(Vector<T> &vector1, Vector<T> &vector2){
    vector1.swap(vector2);
}

template<class T>
size_t Vector<T>::size() {
    return _size;
}

template<class T>
T *Vector<T>::begin() {
    return data;
}

template<class T>
T *Vector<T>::end() {
    return data + _size;
}

template<class T>
void Vector<T>::resize(size_t newSize) {
    T *newData = new T[newSize];
    for (size_t i = 0; i < _size; i++){
        newData[i] = std::move(data[i]);
    }
    delete[] data;
    data = newData;
    size_t oldSize = _size;
    _size = newSize;
    std::cout << "Resized from " << oldSize << " to " << newSize << "\n";
}

template<class T>
void Vector<T>::push_back(const T &element) {
    resize(_size+1);
    data[_size - 1] = element;
    std::cout << "Pushed back new element by copy\n";
}

template<class T>
Vector<T>::Vector() {
    data = nullptr;
    _size = 0;
    std::cout << "Vector default constructor\n";
}

std::ostream &operator<<(std::ostream &out, const Vector<int> &vector) {
    out << "Vector = ";
    if (vector._size == 0){
        out << "[]";
    }
    for (size_t i = 0; i < vector._size; i++){
        out << "[" << vector.data[i] << "] ";
    }

    return out;
}

std::ostream &operator<<(std::ostream &out, const Vector<std::string> &vector) {
    out << "Vector = ";
    if (vector._size == 0){
        out << "[]";
    }
    for (size_t i = 0; i < vector._size; i++){
        out << "[" << vector.data[i] << "] ";
    }

    return out;
}

template<class T>
T &Vector<T>::operator[](size_t index) {
    return data[index];
}

template<class T>
void Vector<T>::getInfo() {
    std::cout << "Vector info:\n";
    std::cout << "\tSize of vector = " << _size << "\n";
    std::cout << "\t" << *this << "\n\n";
}

template<class T>
void Vector<T>::push_back(T &&element) {
    resize(_size+1);
    data[_size - 1] = std::move(element);
    std::cout << "Pushed back new element by move\n";
}

template<class T>
Vector<T>::Vector(const Vector<T> &vector) {
    _size = vector._size;
    data = new T[_size];
    for (unsigned int i = 0; i < _size; i++){
        data[i] = vector.data[i];
    }
    std::cout << "Vector copy constructor\n";
}

template<class T>
Vector<T>::~Vector() {
    delete[] data;
}

template<class T>
Vector<T>::Vector(Vector<T> &&vector) noexcept {
    delete[] data;
    _size = std::move(vector._size);
    data = std::move(vector.data);

    std::cout << "Vector move constructor\n";
}

template<class T>
Vector<T> &Vector<T>::operator=(const Vector<T> &vector) {
    delete[] data;
    _size = vector._size;
    data = new T[_size];
    for (size_t i = 0; i < _size; i++){
        data[i] = vector.data[i];
    }

    std::cout << "Copy assignment\n";
    return *this;
}

template<class T>
Vector<T> &Vector<T>::operator=(Vector<T> &&vector) noexcept{
    swap(vector); // Тогда тогда сработает swap(lvalue)?

    std::cout << "Move assignment\n";
    return *this;
}

template<class T>
void Vector<T>::erase(T *from, T *to) {
    size_t diff = 1;
    T *iterator = from;
    while (iterator != to){
        iterator++;
        diff++;
    }
    T *newData = new T[_size - diff];
    iterator = begin();
    size_t  index = 0;
    while (iterator != from){
        newData[index] = std::move(*iterator);
        iterator++;
        index++;
    }
    iterator = to+1;
    while (iterator != end()){
        newData[index] = std::move(*iterator);
        iterator++;
        index++;
    }

    _size = _size - diff;
    delete[] data;
    data = newData;

    std::cout << "Erased\n";
}

template<class T>
void Vector<T>::pop_back() {
    resize(_size-1);
    std::cout << "Pop back\n";
}

template<class T>
void Vector<T>::swap(Vector<T> &vector) {
    std::swap(*this, vector);
}

#endif //VECTOR_VECTOR_H
