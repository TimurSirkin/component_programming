#include <iostream>
#include <fstream>
#include <algorithm>
#include <random>
#include "Vector.h"

int main() {
    Vector<std::ofstream> vector;
    vector.push_back(std::ofstream("../test/a.txt"));
    vector.push_back(std::ofstream("../test/b.txt"));
    vector.push_back(std::ofstream("../test/c.txt"));
    std::shuffle(vector.begin(), vector.end(), std::random_device());
    size_t i = 0;
    for (std::ofstream &out : vector){
        out << i;
        i++;
        out.close();
    }
    return 0;
}