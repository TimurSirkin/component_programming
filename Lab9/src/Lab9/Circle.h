//
// Created by timur on 22.10.2019.
//

#ifndef LAB6_CIRCLE_H
#define LAB6_CIRCLE_H

#define PI 3.14

template<typename T>
class Circle {
public:
    explicit Circle(T radius);

    void setRadius(T radius);
    T getRadius();

    T getArea();
private:
    T _radius;
};

template<typename T>
Circle<T>::Circle(T radius) {
    _radius = radius;
}

template<typename T>
void Circle<T>::setRadius(T radius) {
    _radius = radius;
}

template<typename T>
T Circle<T>::getRadius() {
    return _radius;
}

template<typename T>
T Circle<T>::getArea() {
    return PI * _radius * _radius;
}

#endif //LAB6_CIRCLE_H
