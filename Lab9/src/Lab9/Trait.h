//
// Created by timur on 22.10.2019.
//

#ifndef LAB6_TRAIT_H
#define LAB6_TRAIT_H

#include <iostream>
#include "Rectangle.h"
#include "Circle.h"

template <class Object>
class Trait{
public:
    static size_t size(const Object &object);
};

template<class Object>
size_t Trait<Object>::size(const Object &object) {
    return sizeof(Object);
}

#endif //LAB6_TRAIT_H
