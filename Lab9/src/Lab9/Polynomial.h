//
// Created by timur on 06.11.2019.
//

#ifndef LAB6_POLYNOMIAL_H
#define LAB6_POLYNOMIAL_H

#include <vector>
#include <complex>

template <typename T>
class Polynomial{
public:
    Polynomial() = default;
    explicit Polynomial(size_t size);
    explicit Polynomial(std::vector<T> &coeffs);
    explicit Polynomial(std::vector<T> &&coeffs);
    void print();
    Polynomial<T> shura();

    template<typename Q = T>
    typename std::enable_if<std::is_fundamental<Q>::value, Q>::type conj(Q coeff);

    template<typename Q = T>
    typename std::enable_if<!std::is_fundamental<Q>::value, Q>::type conj(Q coeff);

private:
    std::vector<T> _coeffs;
};

template<typename T>
Polynomial<T>::Polynomial(std::vector<T> &coeffs) {
    _coeffs = coeffs;
}

template<typename T>
Polynomial<T>::Polynomial(std::vector<T> &&coeffs) {
    _coeffs = std::move(coeffs);
}

template<typename T>
void Polynomial<T>::print() {
    std::cout << "Polynomial:\nP(z) = ";
    for (size_t i = _coeffs.size()-1; i >= 1; --i){
        std::cout << "(" <<  _coeffs[i] << " * z^" << i << ") + ";
    }
    std::cout << "(" << _coeffs[0]  << ")\n";
}

template<typename T>
Polynomial<T> Polynomial<T>::shura() {

    size_t n = _coeffs.size() - 1; // new size
    Polynomial<T> pol(n);

    T A0 = conj(_coeffs[0]);
    T An = _coeffs[n];

    for (size_t k = 0; k < n; k++){

        T Ak = _coeffs[k];
        T Ank = conj(_coeffs[n - k]);

        pol._coeffs[k] = A0*Ak - An*Ank;
    }
    return pol;
}

template<typename T>
template<typename Q>
typename std::enable_if<std::is_fundamental<Q>::value, Q>::type Polynomial<T>::conj(Q coeff) {
    return coeff;
}

template<typename T>
Polynomial<T>::Polynomial(size_t size) {
    _coeffs.resize(size);
}

template<typename T>
template<typename Q>
typename std::enable_if<!std::is_fundamental<Q>::value, Q>::type Polynomial<T>::conj(Q coeff) {
    return std::conj(coeff);
}

#endif //LAB6_POLYNOMIAL_H
