#ifndef LAB5_ITERATOR_H
#define LAB5_ITERATOR_H

#include <iostream>
#include <memory>

template <typename T>
class out_iterator : public std::iterator<std::output_iterator_tag, T>{
public:
    out_iterator(unsigned int N, std::ostream &stream, const std::string &delim);

    out_iterator<T> &operator=(const T &value);
    out_iterator<T> &operator++();
    out_iterator<T> &operator++(int);
    out_iterator<T> &operator* ();
private:
    unsigned int _count = 1;
    unsigned int _printCount;
    std::string _delim;
    std::ostream *_stream;
};

template<typename T>
out_iterator<T> &out_iterator<T>::operator=(const T &value) {
   if (_count < _printCount){
       _count++;
   } else{
       *_stream << _delim << value;
       _count = 1;
   }
   return *this;
}

template<typename T>
out_iterator<T>::out_iterator(unsigned int N, std::ostream &stream, const std::string &delim) {
    _printCount = N;
    _delim = delim;
    _stream = &stream;
}

template<typename T>
out_iterator<T> &out_iterator<T>::operator++(int) {
    return *this;
}

template<typename T>
out_iterator<T> &out_iterator<T>::operator++() {
    // if (_count < _printCount){
    //     _count++;
    // } else{
    //     *_stream << _value << " ";
    //     _count = 1;
    // }
    return *this;
}

template<typename T>
out_iterator<T> &out_iterator<T>::operator*() {
    return *this;
}

template<typename It>
void sort3(It begin, It end){
    size_t size = end - begin;

    if (size > 1) {
        It l_end = begin + size / 2;
        It l_idx = begin;
        It r_idx = l_end;

        sort3(begin, l_end);
        sort3(r_idx, end);

        typedef typename std::iterator_traits<It>::value_type valueType;
        std::vector<valueType> vector(size);
        size_t idx = 0;

        while ((l_idx != l_end) && (r_idx != end)) {
            if (*l_idx < *r_idx){
                vector[idx] = *l_idx;
                idx++;
                l_idx++;
            } else{
                vector[idx] = *r_idx;
                idx++;
                r_idx++;
            }
        }

        if (l_idx == l_end){
            std::copy(r_idx, end, &vector[idx]);
        }
        if (r_idx == end){
            std::copy(l_idx, l_end, &vector[idx]);
        }

        for (const valueType &elem : vector){
            *begin = std::move(elem);
            begin++;
        }
    }
}

#endif //LAB5_ITERATOR_H
