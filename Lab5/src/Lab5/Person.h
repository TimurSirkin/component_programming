//
// Created by timur on 11.10.2019.
//

#ifndef LAB5_PERSON_H
#define LAB5_PERSON_H

#include <iostream>

class Person{
public:
    unsigned int id;
    std::string name;
    friend bool operator<(const Person &person1, const Person &person2);
    bool operator==(const Person& p) const;
private:
};

bool operator<(const Person &person1, const Person &person2) {
    return (person1.id < person2.id);
}

bool Person::operator==(const Person &p) const {
    return id == p.id;
}

namespace std {
    template <>
    struct hash<Person>
    {
        std::size_t operator()(const Person& person) const
        {
            using std::size_t;
            using std::hash;
            using std::string;

            return std::hash<unsigned int>()(person.id);
        }
    };
}

#endif //LAB5_PERSON_H
