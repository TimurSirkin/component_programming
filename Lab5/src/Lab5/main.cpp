#include <iostream>
#include <vector>
#include <map>
#include <unordered_map>
#include <bits/unordered_map.h>
#include "Iterator.h"
#include "Person.h"

void itTest(){
    std::vector<int> v{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::ostream out(std::cout.rdbuf());
    out_iterator<int> it(1, out, "\n");

    std::copy(v.begin(), v.end(), it);
    std::cout << "\nIterator test passed\n";
}

void perTest(){
    Person person1{1, "Timur"};
    Person person2{2, "Timur"};
    Person person3{3, "Timur"};
    Person person4{4, "Timur"};

    std::map<Person, int> map;
    map[person1] = 1;
    map[person2] = 2;
    map[person3] = 3;

    std::unordered_map<Person, int> u_map{};
    u_map.insert({person1, 1});
    u_map.insert({person2, 2});
    u_map[person3] = 3;
    u_map[person3] = 4;

    std::cout << "Person test passed\n";
}

void sortTest(){
    std::vector<int> vector{1, 5, 3, 2, 10, 3, 11, 4, 12};
    sort3(vector.begin(), vector.end());
    for (int elem : vector){
        std::cout << elem << " ";
    }
    std::cout << "\n";

    std::cout << "Sort test passed\n";
}

int main() {
    itTest();
    perTest();
    sortTest();

    return 0;
}